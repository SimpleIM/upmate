<?php
/**
 * Upmate plugin for Craft CMS
 *
 * Upmate Controller
 *
 * --snip--
 * Generally speaking, controllers are the middlemen between the front end of the CP/website and your plugin’s
 * services. They contain action methods which handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering post data, saving it on a model,
 * passing the model off to a service, and then responding to the request appropriately depending on the service
 * method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what the method does (for example,
 * actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 * --snip--
 *
 * @author    Simple
 * @copyright Copyright (c) 2016 Simple
 * @link      https://www.simple.com.au
 * @package   Upmate
 * @since     0.1
 */

namespace Craft;

class UpmateController extends BaseController
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = array('actionIndex',
        );

    /**
     * Handle a request going to our plugin's index action URL, e.g.: actions/upmate
     */
    public function actionIndex()
    {
        $uniqueKey = craft()->config->get('environmentVariables')['uniqueKey'];

        if(craft()->request->getParam('key') == $uniqueKey) {

            try
            {
                $updates = craft()->updates->getUpdates(true);
            }
            catch (EtException $e)
            {
                if ($e->getCode() == 10001)
                {
                    $this->returnErrorJson($e->getMessage());
                }
            }

            if ($updates)
            {
                $response = $updates->getAttributes();
                $response['allowAutoUpdates'] = craft()->config->allowAutoUpdates();
                headerHelper::setHeader([
                    'Access-Control-Allow-Origin' => '*'
                ]);

                $this->returnJson($response);
            }
            else
            {
                $this->returnErrorJson(Craft::t('Could not fetch available updates at this time.'));
            }

        }
        else {
            $this->returnJson('Key Error');
        }
    }
}