# Upmate plugin for Craft CMS

Manage your updates.

## Installation

To install Upmate, follow these steps:

1. Download & unzip the file and place the `upmate` directory into your `craft/plugins` directory
2.  -OR- do a `git clone upmate` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /upmate`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `upmate` for Craft to see it.

Upmate works on Craft 2.4.x and Craft 2.5.x.

## Configuring Upmate

Generate a secure key in settings. The endpoint is then accessible using this key.

## Upmate Roadmap

Some things to do, and ideas for potential features:

* Trim output?

## Upmate Changelog

### 0.1 -- 2016.10.28

* Initial release

Brought to you by [Simple](https://www.simple.com.au)
